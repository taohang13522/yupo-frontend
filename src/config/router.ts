import * as VueRouter from 'vue-router';


/*import Index from "../pages/Index.vue";
import TeamPage from "../pages/TeamPage.vue";
import UserPage from "../pages/UserPage.vue";
import SearchPage from "../pages/SearchPage.vue";
import UserEditPage from "../pages/UserEditPage.vue";
import SearchResultPage from "../pages/SearchResultPage.vue";
import UserLoginPage from "../pages/UserLoginPage.vue";
import TeamAddPage from "../pages/TeamAddPage.vue";
import TeamUpdatePage from "../pages/TeamUpdatePage.vue";
import UserTeamJoinPage from "../pages/UserTeamJoinPage.vue";
import UserTeamCreatePage from "../pages/UserTeamCreatePage.vue";
import UserUpdatePage from "../pages/UserUpdatePage.vue";
import ShowUser from "../pages/ShowUser.vue";
import ShowTeam from "../pages/ShowTeam.vue";*/

const routes = [
    {
        path: '/test',
        meta: {title: "首页"},
        component: () => import("../pages/Test.vue")
    },
    {
        path: '/',
        meta: {title: "首页"},
        component: () => import("../pages/Index.vue")
    },
    {
        path: '/team',
        meta: {title: "队伍",},
        component: () => import("../pages/TeamPage.vue")
    },
    {
        path: '/team/show',
        meta: {title: "查看队伍",},
        name: 'teamShow',
        component: () => import("../pages/ShowTeam.vue")
    },
    {
        path: '/team/add',
        meta: {title: "创建队伍",},
        name: 'teamCreate',
        component: () => import("../pages/TeamAddPage.vue")
    },
    {
        path: '/team/update',
        meta: {title: "修改队伍"},
        name: 'teamEdit',
        component: () => import("../pages/TeamUpdatePage.vue")
    },
    {
        path: '/user',
        meta: {title: "我的"},
        component: () => import("../pages/UserPage.vue")
    },
    {
        path: '/user/login',
        meta: {title: "登录",},
        component: () => import("../pages/UserLoginPage.vue")
    },

    {
        path: '/user/show',
        meta: {title: "查看用户",},
        name: 'userShow',
        component: () => import("../pages/ShowUser.vue")
    },
    {
        path: '/user/team/join',
        meta: {title: "加入的队伍",},
        name: "UserTeamJoin",
        component: () => import("../pages/UserTeamJoinPage.vue")
    },
    {
        path: '/user/edit',
        meta: {title: "用户信息修改"},
        component: () => import("../pages/UserEditPage.vue")
    },
    {
        path: '/search',
        meta: {title: "搜索",},
        component: () => import("../pages/SearchPage.vue")
    },
    {
        path: '/user/update',
        meta: {title: "用户更新信息",},
        component: () => import("../pages/UserUpdatePage.vue")
    },
    {
        path: '/updateTags',
        meta: {title: "用户更新标签信息",},
        component: () => import("../pages/UserTagsUpdatePage.vue")
    },
    {
        path: '/user/list',
        meta: {title: "用户列表",},
        component: () => import("../pages/SearchResultPage.vue")
    },
    {
        path: '/user/matchNearby',
        meta: {title: "查找附近在线用户",},
        component: () => import("../pages/MatchNearbyPage.vue")
    },
    {
        path: '/user/list/nearby',
        meta: {title: "附近用户",},
        component: () => import("../pages/MatchNearbyResultPage.vue")
    },
    {
        path: '/user/team/create',
        meta: {title: "创建的队伍",},
        component: () => import("../pages/UserTeamCreatePage.vue")
    },
    {
       path: '/user/register',
       meta: {title: "注册",},
       component: () => import("../pages/RegisterUser.vue")
   },
    {
        path: '/chat',
        meta: {title: "聊天",},
        component: () => import("../pages/ChatPage.vue")
    },
    {
        path: '/public_chat',
        meta: {title: "聊天室",},
        component: () => import("../components/Chat.vue")
    },
    {
        path: '/friends',
        meta: {title: "我的好友",},
        component: () => import("../pages/FriendsPage.vue")
    },
    {
        path: '/apply',
        meta: {title: "新的朋友",},
        component: () => import("../pages/ApplicationRecord.vue")
    },
     {
        path: '/qqCallback',
        meta:{title:"登录缓冲",},
        component: () => import("../pages/QQCallBack.vue")
 },




]



const routers = VueRouter.createRouter({
    // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
    history: VueRouter.createWebHistory(),
    routes // `routes: routes` 的缩写
})

routers.beforeEach((to, from, next) => {
    to.meta.lastRoutePath = from.path;
    next();
});


export default routers;