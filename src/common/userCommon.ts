export const defaultPicture = "https://tse4-mm.cn.bing.net/th/id/OIP-C.TcuMdI1spwBpgb3M3qIu7AHaNJ?w=187&h=333&c=7&r=0&o=5&dpr=1.3&pid=1.7"

export const jsonParseTag = (usersList) => {
    usersList.forEach(user => {
        if (user.tags) {
            user.tags = user.tags ? JSON.parse(user.tags) : '该用户暂未设置';
        }
    })
}