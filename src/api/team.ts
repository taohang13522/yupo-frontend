import { ResponseVO, pageVO } from '@/types/global'
import {
  TeamJoinRequest,
  TeamListRequest,
  TeamQuitRequest,
  TeamRequest
} from '@/types/team'
import { request } from '@/utils/http'

/** 添加队伍 POST /api/team/add */
export async function addTeam(teamRequest: TeamRequest) {
  return await request.post<ResponseVO>('/api/team/add', teamRequest)
}

/** 分页获取所有队伍列表 POST /api/team/list */
export async function getTeamList(teamListRequest: TeamListRequest) {
  return await request.get<ResponseVO>('/api/team/list', {
    params: teamListRequest
  })
}

/** 退出队伍 POST /api/team/quit */
export async function quitTeam(teamQuitRequest: TeamQuitRequest) {
  return await request.post<ResponseVO>('/api/team/quit', teamQuitRequest)
}

/** 加入队伍 POST /api/team/join */
export async function joinTeam(teamJoinRequest: TeamJoinRequest) {
  return await request.post<ResponseVO>('/api/team/join', teamJoinRequest)
}

/** 查看已加入的队伍列表 GET /api/team/listByUserId */
export async function getJoinTeamList(userId: number) {
  return await request.get<ResponseVO>('/api/team/listByUserId', {
    params: { userId }
  })
}

/** 更新队伍信息 POST /api/team/update */
export async function updateTeam(teamRequest: TeamRequest) {
  return await request.post<ResponseVO>('/api/team/update', teamRequest)
}

/** 查看自己创建的队伍列表 GET /api/team/list/my/create */
export async function getCreateTeamList(pageVO: pageVO) {
  return await request.get<ResponseVO>('/api/team/list/my/create', {
    params: pageVO
  })
}

/** 解散队伍 POST /api/team/dismiss */
export async function dismissTeam(teamRequest: TeamRequest) {
  return await request.post<ResponseVO>('/api/team/dismiss', teamRequest)
}
