import { ResponseVO } from '@/types/global'
import { request } from '@/utils/http'
import { UserLoginVO, type userVO } from '@/types/user'

/** 用户信息更新 POST /api/user/userInfo/update */
export async function updateUserInfo(userInfoVO: userVO) {
  return await request.post<ResponseVO>('/api/user/userInfo/update', userInfoVO)
}

/** 用户登录 POST /api/user/login */
export async function login(userLoginVO: UserLoginVO) {
  return await request.post<ResponseVO>('/api/user/login', userLoginVO)
}

/** 获取登录用户信息 GET /api/user/current */
export async function getCurrentUser() {
  return await request.get<ResponseVO>('/api/user/current')
}

/** 退出 GET /api/user/logout */
export async function logout(userId: number) {
  return await request.get<ResponseVO>('/api/user/logout', {
    params: { id: userId }
  })
}
