import { ResponseVO } from '@/types/global'
import { SearchTagsVO } from '@/types/search'
import { request } from '@/utils/http'

/** 搜索结果页上方情话 GET https://api.vvhan.com/api/love */
export async function getLove() {
  return await request.get<any>('https://api.vvhan.com/api/love?type=json')
}

/** 搜索符合标签的用户 POST /api/user/search/tags */
export async function searchUserByTags(searchTagsVO: SearchTagsVO) {
  return await request.post<ResponseVO>('/api/user/search/tags', searchTagsVO)
}

/** 用户历史搜索 GET /api/user/record */
export async function getUserRecord() {
  return await request.get<ResponseVO>('/api/user/record')
}
