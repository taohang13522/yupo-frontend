import { ResponseVO } from '@/types/global'
import { request } from '@/utils/http'

/** 获取推荐用户(非心动) GET /api/user/recommend */
export async function getRecommendUsers() {
  return await request.get<ResponseVO>('/user/recommend')
}

/** 获取推荐用户(心动) GET /api/user/match */
export async function getMatchUsers(matchNum: number) {
  return await request.get<ResponseVO>('/user/match', {
    params: { num: matchNum }
  })
}
