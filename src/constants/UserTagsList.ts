//原数据
let UserTagsList;
/**
 * 用户标签列表
 */
export default UserTagsList = [
    {
        text: '性别',
        children: [
            { text: '男', id: '男' },
            { text: '女', id: '女' },
        ],
    },
    {
        text: '年级',
        children: [
            { text: '高一', id: '高一' },
            { text: '高二', id: '高二' },
            { text: '高三', id: '高三' },
            { text: '大一', id: '大一' },
            { text: '大二', id: '大二' },
            { text: '大三', id: '大三' },
            { text: '大四', id: '大四' },
        ],
    },
    {
        text: '运动',
        children: [
            { text: '篮球', id: '篮球' },
            { text: '足球', id: '足球' },
            { text: '排球', id: '排球' },
            { text: '乒乓球', id: '乒乓球' },
            { text: '网球', id: '网球' },
            { text: '台球', id: '台球' },
            { text: '滑雪', id: '滑雪' },
        ],
    },
    {
        text: '状态',
        children: [
            { text: '求职', id: '求职' },
            { text: '摆烂', id: '摆烂' },
            { text: '工作', id: '工作' },
            { text: '学习', id: '学习' },
        ],
    },
    {
        text: '专业方向',
        children: [
            { text: 'Java', id: 'Java' },
            { text: 'C++', id: 'C++' },
            { text: 'Go', id: 'Go' },
            { text: 'PHP', id: 'PHP' },
            { text: 'Python', id: 'Python' },
            { text: '人工智能', id: '人工智能' },
            { text: '大数据', id: '大数据' },
            { text: '网络安全', id: '网络安全' },
        ],
    },
    {
        text: '兴趣',
        children: [
            { text: '电影', id: '电影' },
            { text: '刷剧', id: '刷剧' },
            { text: '美食', id: '美食' },
            { text: '旅游', id: '旅游' },
            { text: '音乐', id: '音乐' },
            { text: '读书', id: '读书' },
            { text: '艺术', id: '艺术' },
        ],
    },
];

