let tagList;

export default tagList = [
    {
        title: '编程语言',

        content: [
            {id: 1, name: 'java', isActive: false},
            {id: 2, name: 'python', isActive: false},
            {id: 3, name: 'c', isActive: false},
            {id: 4, name: 'c++', isActive: false},
            {id: 5, name: 'c#', isActive: false},
            {id: 6, name: 'javascript', isActive: false},
            {id: 7, name: 'typescript', isActive: false},
            {id: 8, name: 'ruby', isActive: false},
            {id: 9, name: 'go', isActive: false},
            {id: 10, name: 'rust', isActive: false},
            {id: 11, name: 'kotlin', isActive: false},
            {id: 12, name: 'scala', isActive: false},
            {id: 13, name: 'swift', isActive: false},
            {id: 14, name: 'php', isActive: false},
            {id: 15, name: 'perl', isActive: false},
            {id: 16, name: 'objectivec', isActive: false},
            {id: 17, name: 'shell', isActive: false},
            {id: 18, name: 'groovy', isActive: false},
            // {id: 19, name: 'lua', isActive: false},
            // {id: 20, name: 'sql', isActive: false},
            // {id: 21, name: 'html', isActive: false},
            // {id: 22, name: 'css', isActive: false},
            // {id: 23, name: 'sass', isActive: false},
            // {id: 24, name: 'less', isActive: false},
            // {id: 25, name: 'json', isActive: false},
            // {id: 26, name: 'xml', isActive: false},
            // {id: 27, name: 'yaml', isActive: false},
            // {id: 28, name: 'markdown', isActive: false},
            // {id: 29, name: 'bash', isActive: false}
        ]
    },
    {
        title: '学历',

        content: [
            {id: 30, name: '大一', isActive: false},
            {id: 31, name: '大二', isActive: false},
            {id: 32, name: '大三', isActive: false},
            {id: 33, name: '大四', isActive: false},
            {id: 34, name: '研一', isActive: false},
            {id: 35, name: '研二', isActive: false},
            {id: 36, name: '研三', isActive: false}
        ]
    },
    {
        title: '情绪',

        content: [
            {id: 37, name: 'emo', isActive: false},
            {id: 38, name: 'happy', isActive: false},
            {id: 39, name: 'sad', isActive: false},
            {id: 40, name: 'angry', isActive: false},
            {id: 41, name: 'excited', isActive: false},
            {id: 42, name: 'depressed', isActive: false},
            {id: 43, name: 'lonely', isActive: false},
            {id: 44, name: 'bored', isActive: false}
        ]
    },
    {
        title: '运动',

        content: [
            {id: 45, name: '篮球', isActive: false},
            {id: 46, name: '足球', isActive: false},
            {id: 47, name: '排球', isActive: false},
            {id: 48, name: '乒乓球', isActive: false},
            {id: 49, name: '网球', isActive: false},
            {id: 50, name: '台球', isActive: false},
            {id: 51, name: '滑雪', isActive: false}
        ]
    },
];