import { createApp } from 'vue'
import App from './App.vue'
import  Vant from "vant";
import  "vant/lib/index.css"
import './global.css'
import router from "./config/router";

import ArcoVue from '@arco-design/web-vue';
import '@arco-design/web-vue/dist/arco.css';

const app = createApp(App);

app.use(ArcoVue);
app.use(Vant);

app.use(router)

app.mount('#app');




