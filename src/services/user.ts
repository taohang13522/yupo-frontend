import {getCurrentUserState, setCurrentUserState} from "../states/user";
import myAxios from "../plugins/myAxios";

export  const getCurrentUser  =  async ()=>{
    //const user = getCurrentUserState();
    //if(user){
    //    return user;
    //}
    //不存在则从远程获取
    const res = await  myAxios.get("/user/current");
    if(res.code===0){
        setCurrentUserState(res.data);
        return res.data;
    }
    return null;
}